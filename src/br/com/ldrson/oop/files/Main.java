package br.com.ldrson.oop.files;

import java.util.ArrayList;
import java.util.StringJoiner;

/**
 * Created by leanderson on 17/06/16.
 */
public class Main {

    public static void main(String args[]){

        System.out.println("|-----------------------------------------------|");
        System.out.println("|                  Java Files                   |");
        System.out.println("|-----------------------------------------------|");

        exemplosComleituraDeArquivo();

        exemplosComEscritaDeArquivos();

    }

    private static void exemplosComEscritaDeArquivos(){

        EscritorDeArquivo escritorDeArquivo = new EscritorDeArquivo();


        ArrayList<String> linhas = new ArrayList<>();
        linhas.add("Minha primeira linha do arquivo");
        linhas.add("Minha segunda linha do arquivo");
        linhas.add("Minha terceira linha do arquivo");

        escritorDeArquivo.escreverNovoArquivo("teste.txt",linhas);


        ArrayList<String> novasLinhas = new ArrayList<>();
        novasLinhas.add("UMA NOVA LINHA");
        novasLinhas.add("DUAS NOVA LINHA");

        escritorDeArquivo.adicionarNoArquivo("teste.txt", novasLinhas);

        escritorDeArquivo.adicionarNoArquivo("um_novo_arquivo.txt", novasLinhas);

        escritorDeArquivo.salvarListaDeProdutos("nova_lista_de_produtos.txt",obterUmaListaDeProdutos());

        Produto produtoAlterado = new Produto(123,"Livro","Inferno -- 2","DAN Brown",99.99);

        escritorDeArquivo.salvarAlteracaoDoProduto("nova_lista_de_produtos.txt",produtoAlterado);


        escritorDeArquivo.excluirProduto("nova_lista_de_produtos.txt",produtoAlterado);


    }


    private static  void exemplosComleituraDeArquivo(){

        LeitorDeArquivo leitorDeArquivo = new LeitorDeArquivo();

        // Vai lançar um erro
        leitorDeArquivo.lerArquivoPorLinha("arquivo_que_nao_existe.txt");

        leitorDeArquivo.lerArquivoPorCamposDelimitados("assets/lista_de_produtos.txt","&");

        ArrayList<Produto> listaDeProdutos = leitorDeArquivo.lerOsProdutosSalvos("assets/lista_de_produtos.txt");

        System.out.println("|-----------------------------------------------|");
        System.out.println("|    Lista de Produtos Recuperado do Arquivo    |");
        System.out.println("|-----------------------------------------------|");
        for(Produto produto : listaDeProdutos){
            System.out.println(produto);
        }
    }

    private static ArrayList<Produto> obterUmaListaDeProdutos(){
        ArrayList<Produto> lista = new ArrayList<>();
        lista.add(new Produto(99,"Livro","Código da Vinci","Dan Brown",56.78));
        lista.add(new Produto(123,"Livro","Inferno","Dan Brown",45.45));
        lista.add(new Produto(126,"Livro","Símbolo Perdido","Dan Brown",85.45));
        return lista;
    }

}
