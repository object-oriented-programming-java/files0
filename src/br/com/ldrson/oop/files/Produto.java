package br.com.ldrson.oop.files;

/**
 * Created by leanderson on 17/06/16.
 */
public class Produto {

    private long codigo;
    private String tipo;
    private String nome;
    private String autor;
    private double precoUnitario;

    public Produto(){

    }

    public Produto(long codigo, String tipo, String nome, String autor, double precoUnitario) {
        this.codigo = codigo;
        this.tipo = tipo;
        this.nome = nome;
        this.autor = autor;
        this.precoUnitario = precoUnitario;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public double getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(double precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    @Override
    public String toString() {
        return "[Código : "+codigo + ", Tipo : "+tipo+", Nome : "+nome+", Autor : "+autor+", Preço Unitário : "+precoUnitario+"]";
    }

    public String toString(String delimitador) {
        return codigo+delimitador+tipo+delimitador+nome+delimitador+autor+delimitador+precoUnitario;
    }
}
