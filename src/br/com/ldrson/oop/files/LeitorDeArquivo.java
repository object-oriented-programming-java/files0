package br.com.ldrson.oop.files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by leanderson on 17/06/16.
 */
public class LeitorDeArquivo {

    public void lerArquivoPorLinha(String nomeDoArquivo){


        System.out.println("|-----------------------------------------------|");
        System.out.println("|            Arquivo :: "+nomeDoArquivo);
        System.out.println("|-----------------------------------------------|");

        try{

            FileReader fileReader = new FileReader(nomeDoArquivo);
            Scanner scanner = new Scanner(fileReader);

            while (scanner.hasNext()) {

                String linha = scanner.nextLine();

                System.out.println(linha);

            }

        }catch (FileNotFoundException e){
            System.out.println("ERRO :: arquivo com nome \""+nomeDoArquivo+"\" não foi econtrado.");
        }catch (Exception e){
            e.printStackTrace();
        }finally {

        }
    }

    public void lerArquivoPorCamposDelimitados(String nomeDoArquivo,String delimitador){


        System.out.println("|-----------------------------------------------|");
        System.out.println("|            Arquivo :: "+nomeDoArquivo);
        System.out.println("|-----------------------------------------------|");

        try{

            FileReader fileReader = new FileReader(nomeDoArquivo);
            Scanner scanner = new Scanner(fileReader);

            while (scanner.hasNext()) {

                String linha = scanner.nextLine();

                String[] vetor = linha.split(delimitador);

                for(String campo : vetor){
                    System.out.print(campo+"  |  ");
                }
                System.out.println();

            }

        }catch (FileNotFoundException e){
            System.out.println("ERRO :: arquivo com nome \""+nomeDoArquivo+"\" não foi econtrado.");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public ArrayList<Produto> lerOsProdutosSalvos(String nomeDoArquivo){

        String delimitador = "&";

        ArrayList<Produto> lista = new ArrayList<>();

        System.out.println("|-----------------------------------------------|");
        System.out.println("|  Leitura do Arquivo :: "+nomeDoArquivo);
        System.out.println("|-----------------------------------------------|");

        try{

            FileReader fileReader = new FileReader(nomeDoArquivo);
            Scanner scanner = new Scanner(fileReader);

            while (scanner.hasNext()) {

                Produto produto = new Produto();

                String linha = scanner.nextLine();

                String[] vetor = linha.split(delimitador);

                produto.setCodigo(Long.parseLong(vetor[0]));
                produto.setTipo(vetor[1]);
                produto.setNome(vetor[2]);
                produto.setAutor(vetor[3]);
                produto.setPrecoUnitario(Double.parseDouble(vetor[4]));

                lista.add(produto);

            }

        }catch (FileNotFoundException e){
            System.out.println("| ==> ERRO :: arquivo com nome \""+nomeDoArquivo+"\" não foi econtrado.");
        }catch (Exception e){
            e.printStackTrace();
        }

        return lista;
    }

}
