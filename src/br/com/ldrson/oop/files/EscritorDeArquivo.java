package br.com.ldrson.oop.files;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by leanderson on 17/06/16.
 */
public class EscritorDeArquivo {


    public void escreverNovoArquivo(String nomeDoArquivo, ArrayList<String> linhas){

        System.out.println("|-----------------------------------------------|");
        System.out.println("|  Escrita do Arquivo :: "+nomeDoArquivo);
        System.out.println("|  Cria um novo arquivo, caso não existir");
        System.out.println("|  Sobrescreve o arquivo, caso  existir");
        System.out.println("|-----------------------------------------------|");

        try{

            FileWriter fileWriter = new FileWriter(nomeDoArquivo);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for(String linha : linhas) {

                bufferedWriter.write(linha);
                bufferedWriter.newLine();

            }

            bufferedWriter.close();
            fileWriter.close();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void adicionarNoArquivo(String nomeDoArquivo, ArrayList<String> novasLinhas) {

        System.out.println("|-----------------------------------------------|");
        System.out.println("|  Escrita do Arquivo :: "+nomeDoArquivo);
        System.out.println("|  Adiciona as novas linhas no fim do arquivo, caso ele existir ");
        System.out.println("|  Cria um novo arquivo, caso não existir ");
        System.out.println("|-----------------------------------------------|");

        try{

            FileWriter fileWriter = new FileWriter(nomeDoArquivo, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for(String linha : novasLinhas) {

                bufferedWriter.write(linha);
                bufferedWriter.newLine();

            }

            bufferedWriter.close();
            fileWriter.close();

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public void salvarListaDeProdutos(String nomeDoArquivo, ArrayList<Produto> lista) {

        String delimitador = "&";

        ArrayList<String> linhas = new ArrayList<>();
        for(Produto produto : lista){
            linhas.add(produto.toString(delimitador));
        }
        escreverNovoArquivo(nomeDoArquivo,linhas);
    }

    public void salvarAlteracaoDoProduto(String nomeDoArquivo, Produto produto) {

        String delimitador = "&";

        ArrayList<String> linhas = new ArrayList<>();

        try{

            FileReader fileReader = new FileReader(nomeDoArquivo);
            Scanner scanner = new Scanner(fileReader);

            while (scanner.hasNext()) {

                String linha = scanner.nextLine();

                String[] vetor = linha.split(delimitador);

                Long codigoDoProdutoNestaLinha = Long.parseLong(vetor[0]);

                if(codigoDoProdutoNestaLinha == produto.getCodigo()){
                    linhas.add(produto.toString(delimitador));
                }else {
                    linhas.add(linha);
                }

            }

        }catch (FileNotFoundException e){
            System.out.println("| ==> ERRO :: arquivo com nome \""+nomeDoArquivo+"\" não foi econtrado.");
        }catch (Exception e){
            e.printStackTrace();
        }

        escreverNovoArquivo(nomeDoArquivo,linhas);


    }

    public void excluirProduto(String nomeDoArquivo, Produto produto) {
        String delimitador = "&";

        ArrayList<String> linhas = new ArrayList<>();

        try{

            FileReader fileReader = new FileReader(nomeDoArquivo);
            Scanner scanner = new Scanner(fileReader);

            while (scanner.hasNext()) {

                String linha = scanner.nextLine();

                String[] vetor = linha.split(delimitador);

                Long codigoDoProdutoNestaLinha = Long.parseLong(vetor[0]);

                if(codigoDoProdutoNestaLinha != produto.getCodigo()){
                    linhas.add(linha);
                }

            }

        }catch (FileNotFoundException e){
            System.out.println("| ==> ERRO :: arquivo com nome \""+nomeDoArquivo+"\" não foi econtrado.");
        }catch (Exception e){
            e.printStackTrace();
        }

        escreverNovoArquivo(nomeDoArquivo,linhas);
    }
}
